import 'package:flutter_test/flutter_test.dart';
import 'package:freader/domain/feeds/feed.dart';
import 'package:webfeed/domain/atom_feed.dart';
import 'package:webfeed/domain/rss_feed.dart';
import 'package:webfeed/domain/rss_image.dart';

void main() {
  group("Factory coverters", () {
    test("Should convert AtomFeed to Feed", () {
      final atomFeed = AtomFeed(
          title: "title",
          icon: "icon",
          subtitle: "subtitle",
          updated: DateTime.now());
      final expected = Feed(
        url: "dummy",
        type: FeedType.Atom,
        title: "title",
        iconUrl: "icon",
        subTitle: "subtitle",
        updated: atomFeed.updated,
      );
      final feed = Feed.fromAtomFeed("dummy", atomFeed);

      expect(feed, expected);
    });

    test("Should convert RssFeed to Feed", () {
      final dateTime = DateTime.now();
      final rssFeed = RssFeed(
          title: "title",
          image: RssImage(url: "icon"),
          description: "subtitle",
          lastBuildDate: dateTime.toIso8601String());

      final atomFeed = AtomFeed(
        title: "title",
        icon: "icon",
        subtitle: "subtitle",
        updated: dateTime,
      );

      final expectedRss = Feed(
        url: "dummy",
        type: FeedType.RSS,
        title: "title",
        iconUrl: "icon",
        subTitle: "subtitle",
        updated: DateTime.parse(rssFeed.lastBuildDate!),
      );

      final expectedAtom = expectedRss.copyWith(type: FeedType.Atom);
      final rssFeedConverted = Feed.fromParsedFeed("dummy", rssFeed);
      final atomFeedConverted = Feed.fromParsedFeed("dummy", atomFeed);

      expect(rssFeedConverted, expectedRss);
      expect(atomFeedConverted, expectedAtom);
    });

    test("Should convert RSS or Atom to Feed", () {
      final rssFeed = RssFeed(
          title: "title",
          image: RssImage(url: "icon"),
          description: "subtitle",
          lastBuildDate: DateTime.now().toIso8601String());
      final expected = Feed(
        url: "dummy",
        type: FeedType.RSS,
        title: "title",
        iconUrl: "icon",
        subTitle: "subtitle",
        updated: DateTime.parse(rssFeed.lastBuildDate!),
      );
      final feed = Feed.fromRssFeed("dummy", rssFeed);

      expect(feed, expected);
    });
  });

  group("Json", () {
    test("Should convert toJson", () {
      final dateTime = DateTime.parse("2021-05-07T15:22:36.627652");
      final rssFeed = RssFeed(
          title: "title",
          image: RssImage(url: "icon"),
          description: "subtitle",
          lastBuildDate: dateTime.toIso8601String());
      final feed = Feed(
        url: "dummy",
        type: FeedType.RSS,
        title: "title",
        iconUrl: "icon",
        subTitle: "subtitle",
        updated: DateTime.parse(rssFeed.lastBuildDate!),
      );
      final expected = {
        'url': 'dummy',
        'type': 'RSS',
        'iconUrl': 'icon',
        'title': 'title',
        'subTitle': 'subtitle',
        'updated': '2021-05-07T15:22:36.627652'
      };

      final json = feed.toJson();
      expect(json, expected);
    });

    test("Should convert fromJson", () {
      final dateTime = DateTime.parse("2021-05-07T15:22:36.627652");
      final rssFeed = RssFeed(
          title: "title",
          image: RssImage(url: "icon"),
          description: "subtitle",
          lastBuildDate: dateTime.toIso8601String());
      final expected = Feed(
        url: "dummy",
        type: FeedType.RSS,
        title: "title",
        iconUrl: "icon",
        subTitle: "subtitle",
        updated: DateTime.parse(rssFeed.lastBuildDate!),
      );
      final json = {
        'url': 'dummy',
        'type': 'RSS',
        'iconUrl': 'icon',
        'title': 'title',
        'subTitle': 'subtitle',
        'updated': '2021-05-07T15:22:36.627652'
      };

      final feed = Feed.fromJson(json);
      expect(feed, expected);
    });
  });
}
