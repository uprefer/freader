/// *** GENERATED FILE - ANY CHANGES WOULD BE OBSOLETE ON NEXT GENERATION *** ///

/// Helper to test coverage for all project files
// ignore_for_file: unused_import
import 'package:freader/application/feeds/add_feed_cubit.dart';
import 'package:freader/application/feeds/feeds_cubit.dart';
import 'package:freader/application/feeds/add_feed_state.dart';
import 'package:freader/application/feeds/feeds_state.dart';
import 'package:freader/infrastructure/core/injectable_module.dart';
import 'package:freader/infrastructure/core/iterable_extension.dart';
import 'package:freader/infrastructure/core/errors.dart';
import 'package:freader/infrastructure/feeds/feed_remote_data_source.dart';
import 'package:freader/infrastructure/feeds/feed_facade.dart';
import 'package:freader/infrastructure/feeds/feed_identifier.dart';
import 'package:freader/infrastructure/feeds/feed_local_data_source.dart';
import 'package:freader/main.dart';
import 'package:freader/domain/feeds/feed.dart';
import 'package:freader/domain/feeds/feed_failure.dart';
import 'package:freader/domain/feeds/i_feed_facade.dart';
import 'package:freader/injection.dart';
import 'package:freader/presentation/core/router.dart';
import 'package:freader/presentation/home_screen.dart';
import 'package:freader/presentation/feeds/add_feed_screen.dart';

void main() {}
