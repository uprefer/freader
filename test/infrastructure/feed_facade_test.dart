import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:freader/domain/feeds/feed.dart';
import 'package:freader/domain/feeds/feed_failure.dart';
import 'package:freader/infrastructure/core/errors.dart';
import 'package:freader/infrastructure/feeds/feed_remote_data_source.dart';
import 'package:freader/infrastructure/feeds/feed_facade.dart';
import 'package:freader/infrastructure/feeds/feed_local_data_source.dart';
import 'package:hive/hive.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'feed_facade_test.mocks.dart';

@GenerateMocks([FeedRemoteDataSource, FeedLocalDataSource])
void main() {
  MockFeedRemoteDataSource remoteDataSource = MockFeedRemoteDataSource();
  MockFeedLocalDataSource localDataSource = MockFeedLocalDataSource();
  Directory tempDBDir = Directory("./dbTest");

  setUpAll(() {
    if (!tempDBDir.existsSync()) {
      tempDBDir.createSync();
    }
    Hive.init("./dbTest");
  });

  tearDown(() async {
    final box = await Hive.openBox("feeds");
    await box.deleteFromDisk();
  });

  group("feedFromUrl", () {
    test("Should return feed", () async {
      final feed = Feed(url: "", type: FeedType.RSS, title: "");
      when(remoteDataSource.get("")).thenAnswer(
        (_) => Future.value(feed),
      );
      final facade = FeedFacade(remoteDataSource, localDataSource);
      final failOrSuccess = await facade.feedFromUrl("");
      failOrSuccess.fold(
        (l) => expect(false, true, reason: "Should not be a failure !"),
        (r) => expect(r, feed),
      );
    });

    test('Should return FeedFailure.networkError() if DioError', () async {
      when(remoteDataSource.get("")).thenThrow(DioError(
        requestOptions: RequestOptions(path: ""),
      ));
      final facade = FeedFacade(remoteDataSource, localDataSource);

      final actual = await facade.feedFromUrl("");
      actual.fold(
        (l) => expect(l, FeedFailure.networkError()),
        (r) => expect(false, true, reason: "Should be a failure !"),
      );
    });

    test('Should return FeedFailure.invalidFeed() if FeedTypeError', () async {
      when(remoteDataSource.get("")).thenThrow(FeedTypeError());
      final facade = FeedFacade(remoteDataSource, localDataSource);

      final actual = await facade.feedFromUrl("");
      actual.fold(
        (l) => expect(l, FeedFailure.invalidFeed()),
        (r) => expect(false, true, reason: "Should be a failure !"),
      );
    });

    test('Should return failure if FeedTypeError', () async {
      final expectedError = ArgumentError();
      when(remoteDataSource.get("")).thenThrow(expectedError);
      final facade = FeedFacade(remoteDataSource, localDataSource);

      final actual = await facade.feedFromUrl("");
      actual.fold(
        (l) => expect(l, FeedFailure.unknown(expectedError)),
        (r) => expect(false, true, reason: "Should be a failure !"),
      );
    });
  });

  group("save", () {
    test('Should save Feed', () async {
      final feed = Feed(url: "dummy", type: FeedType.Atom, title: "");

      final facade = FeedFacade(remoteDataSource, FeedLocalDataSource());
      await facade.save(feed);

      final savedFeed = await facade.get(feed.url);
      savedFeed.fold(
        (l) => expect(false, true, reason: "Should not be a failure !"),
        (r) => expect(r, feed),
      );
    });

    test('Should return FeedFailure.unknown if save error', () async {
      final feed = Feed(url: "dummy", type: FeedType.Atom, title: "");
      final expectedError = ArgumentError();
      when(localDataSource.save(feed)).thenThrow(expectedError);
      final facade = FeedFacade(remoteDataSource, localDataSource);
      await facade.save(feed);

      final savedFeed = await facade.save(feed);
      savedFeed.fold(
        (l) => expect(l, FeedFailure.unknown(expectedError)),
        (r) => expect(false, true, reason: "Should be a failure !"),
      );
    });
  });

  group("getAll", () {
    test('Should return FeedFailure.unknown if error', () async {
      final expectedError = ArgumentError();
      when(localDataSource.getAll()).thenThrow(expectedError);
      final facade = FeedFacade(remoteDataSource, localDataSource);

      final savedFeed = await facade.getAll();
      savedFeed.fold(
        (l) => expect(l, FeedFailure.unknown(expectedError)),
        (r) => expect(false, true, reason: "Should be a failure !"),
      );
    });

    test('Should return feed list', () async {
      final feed = List<Feed>.generate(
        10,
        (index) => Feed(url: "$index", type: FeedType.Atom, title: ""),
      );

      when(localDataSource.getAll())
          .thenAnswer((realInvocation) => Future.value(feed));
      final facade = FeedFacade(remoteDataSource, localDataSource);
      final savedFeed = await facade.getAll();
      savedFeed.fold(
        (l) => expect(false, true, reason: "Should not be a failure !"),
        (r) => expect(r, feed),
      );
    });
  });

  group("get", () {
    test("Should return FeedFailure.notFound if feed is not on local storage",
        () async {
      when(localDataSource.get(url: "notFound"))
          .thenThrow(LocalStorageNotFoundError());
      final facade = FeedFacade(remoteDataSource, localDataSource);
      final savedFeed = await facade.get("notFound");
      savedFeed.fold(
        (l) => expect(l, FeedFailure.notFound()),
        (r) => expect(false, true, reason: "Should be a failure !"),
      );
    });
  });
}
