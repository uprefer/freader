import 'package:flutter_test/flutter_test.dart';
import 'package:freader/domain/feeds/feed.dart';
import 'package:freader/infrastructure/core/errors.dart';
import 'package:freader/infrastructure/feeds/feed_identifier.dart';

import 'consts.dart';

void main() {
  group("isRSSFeed", () {
    test("Should return true if feed is RSS", () {
      expect(
        FeedIdentifier.isRSSFeed(RSSString),
        true,
      );
    });

    test("Should return false if is Atom", () {
      expect(
        FeedIdentifier.isRSSFeed(AtomString),
        false,
      );
    });
  });
  group("isAtomFeed", () {
    test("Should return false if feed is RSS", () {
      expect(
        FeedIdentifier.isAtomFeed(RSSString),
        false,
      );
    });

    test("Should return true if feed is Atom", () {
      expect(
        FeedIdentifier.isAtomFeed(AtomString),
        true,
      );
    });
  });

  group("identifyFeedType", () {
    test("Should return FeedType.RSS if feed is an RSS", () {
      expect(
        FeedIdentifier.identifyFeedType(RSSString),
        FeedType.RSS,
      );
    });

    test("Should return FeedType.Atom if feed is an Atom", () {
      expect(
        FeedIdentifier.identifyFeedType(AtomString),
        FeedType.Atom,
      );
    });

    test("Should throw FeedTypeError if is not RSS or Atom", () {
      expect(
        () => FeedIdentifier.identifyFeedType("<?xml ?><nothing></nothing>"),
        throwsA(isInstanceOf<FeedTypeError>()),
      );
    });
  });
}
