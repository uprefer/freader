import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:freader/domain/feeds/feed.dart';
import 'package:freader/infrastructure/core/errors.dart';
import 'package:freader/infrastructure/feeds/feed_remote_data_source.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'consts.dart';
import 'feed_remote_data_source_test.mocks.dart';

@GenerateMocks([Dio])
void main() {
  test("Should throw DioException if url is empty", () async {
    final dataSource = FeedRemoteDataSource(Dio());

    expect(() async => await dataSource.get(""),
        throwsA(isInstanceOf<DioError>()));
  });

  test("Should throw DioException if bad url", () async {
    final dataSource = FeedRemoteDataSource(Dio());

    expect(() async => await dataSource.get("http://b.b.b.b.b.b.b."),
        throwsA(isInstanceOf<DioError>()));
  });

  test("Should throw FeedTypeError if resource is not Atom or Rss Feed", () {
    final dataSource = FeedRemoteDataSource(Dio());
    expect(
        () async => dataSource.get("https://google.com"),
        throwsA(
          isInstanceOf<FeedTypeError>(),
        ));
  });

  test("Should return well formatted Feed for RSS", () async {
    final mockedDio = MockDio();
    when(mockedDio.get("")).thenAnswer((_) => Future.value(
          Response(
            data: RSSString,
            requestOptions: RequestOptions(path: ""),
          ),
        ));
    final dataSource = FeedRemoteDataSource(mockedDio);
    final expected = Feed(
      url: "",
      type: FeedType.RSS,
      iconUrl:
          "https://korben.info/app/uploads/2019/07/cropped-android-chrome-512x512-32x32.png",
      title: "Korben",
      subTitle: "Upgrade your mind",
      updated: null,
    );
    final actual = await dataSource.get("");
    expect(actual, expected);
  });

  test("Should return well formatted Feed for Atom", () async {
    final mockedDio = MockDio();
    when(mockedDio.get("")).thenAnswer((_) => Future.value(
          Response(
            data: AtomString,
            requestOptions: RequestOptions(path: ""),
          ),
        ));
    final dataSource = FeedRemoteDataSource(mockedDio);
    final expected = Feed(
        url: "",
        type: FeedType.Atom,
        title: "Atomic Starling",
        updated: DateTime.parse("2007-02-25 13:37:36.000Z"));
    final actual = await dataSource.get("");
    expect(actual, expected);
  });
}
