import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:freader/domain/feeds/feed.dart';
import 'package:freader/infrastructure/core/errors.dart';
import 'package:freader/infrastructure/feeds/feed_local_data_source.dart';
import 'package:hive/hive.dart';

void main() {
  Directory tempDBDir = Directory("./dbTest");
  final FeedLocalDataSource dataSource = FeedLocalDataSource();

  setUpAll(() {
    if (!tempDBDir.existsSync()) {
      tempDBDir.createSync();
    }
    Hive.init("./dbTest");
  });

  tearDown(() async {
    var box = await Hive.openBox("feeds");
    await box.deleteFromDisk();
  });

  test("Should save Atom feed", () async {
    final feed = Feed(url: "", type: FeedType.Atom, title: "");
    await dataSource.save(feed);

    final savedFeed = await dataSource.get(url: feed.url);
    expect(savedFeed, feed);
  });

  test("Should save RSS feed", () async {
    final feed = Feed(url: "", type: FeedType.RSS, title: "");
    await dataSource.save(feed);

    final savedFeed = await dataSource.get(url: feed.url);
    expect(savedFeed, feed);
  });

  test("Should throw Error if feed not found", () async {
    expect(
      () async => await dataSource.get(url: "notFound"),
      throwsA(isInstanceOf<LocalStorageNotFoundError>()),
    );
  });

  test("Should get all Feed in local storage", () async {
    final feeds = List<Feed>.generate(
        10,
        (index) => Feed(
            url: '$index',
            type: index.isEven ? FeedType.Atom : FeedType.RSS,
            title: ""));

    feeds.forEach((element) async {
      await dataSource.save(element);
    });

    final savedFeeds = await dataSource.getAll();

    expect(savedFeeds, feeds);
  });

  test("Should add multiple feeds in local storage", () async {
    final feeds = List<Feed>.generate(
        10,
        (index) => Feed(
            url: '$index',
            type: index.isEven ? FeedType.Atom : FeedType.RSS,
            title: ""));

    await dataSource.saveAll(feeds);
    final savedFeeds = await dataSource.getAll();
    expect(savedFeeds, feeds);
  });
}
