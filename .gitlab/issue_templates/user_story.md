# DOR: Definition of ready
* [ ] Contrat d'interface défini
* [ ] US rédigée
* [ ] Jeux de données validés
* [ ] UI Validée
* [ ] Critères d'acceptations rédigés

# DOD: Definition of done 

* [ ] UI
* [ ] Bloc couvert par des TU
* [ ] MR validée avec relecture
* [ ] Tests utilisateurs validés
* [ ] Critères d'acceptations joués et validés

# User Story

Rédiger l'US ici

# Critères d'acceptations

Liste des critères d'acceptations
