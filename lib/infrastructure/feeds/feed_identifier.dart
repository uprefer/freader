import '../../domain/feeds/feed.dart';
import '../core/errors.dart';
import 'package:xml/xml.dart';
import '../core/iterable_extension.dart';


class FeedIdentifier {
  static bool isRSSFeed(String feedData) {
    final document = XmlDocument.parse(feedData);
    final rss = document.findElements('rss').firstOrNull;
    final rdf = document.findElements('rdf:RDF').firstOrNull;
    return (rss != null || rdf != null);
  }

  static bool isAtomFeed(String feedData) {
    final document = XmlDocument.parse(feedData);
    final feedElement = document.findElements('feed').firstOrNull;
    return feedElement != null;
  }

  static FeedType identifyFeedType(String feedData) {
    try {
      if (isRSSFeed(feedData)) {
        return FeedType.RSS;
      }

      if (isAtomFeed(feedData)) {
        return FeedType.Atom;
      }
      return throw FeedTypeError();
    } on XmlParserException catch (_) {
      throw FeedTypeError();
    }
  }
}
