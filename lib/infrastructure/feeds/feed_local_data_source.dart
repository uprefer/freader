import 'dart:async';

import 'package:dartz/dartz.dart';
import '../../domain/feeds/feed.dart';
import '../core/errors.dart';
import 'package:hive/hive.dart';
import 'package:injectable/injectable.dart';



@lazySingleton
class FeedLocalDataSource {
  static const _boxName = "feeds";
  final StreamController<Unit> feedStream = StreamController.broadcast();
  Future<Box> get _box async {
    if (Hive.isBoxOpen(_boxName)) {
      return Hive.box(_boxName);
    } else {
      return await Hive.openBox(_boxName);
    }
  }

  Future<void> save(Feed feed) async {
    final feedJson = feed.toJson();
    final box = await _box;
    await box.put(feed.url, feedJson);
    feedStream.add(unit);
  }

  Future<void> saveAll(List<Feed> feeds) async {
    feeds.forEach((f) async {
      await save(f);
    });
  }

  Future<List<Feed>> getAll() async {
    final box = await _box;
    return box.values
        .map((e) => Feed.fromJson(Map<String, dynamic>.from(e)))
        .toList();
  }

  Future<Feed> get({required String url}) async {
    final box = await _box;
    final raw = box.get(url);
    if (raw == null) {
      throw LocalStorageNotFoundError();
    }
    return Feed.fromJson(raw);
  }
}
