import 'dart:async';

import 'package:dio/dio.dart';
import '../../domain/feeds/feed.dart';
import 'feed_identifier.dart';
import 'package:injectable/injectable.dart';
import 'package:webfeed/domain/atom_feed.dart';
import 'package:webfeed/domain/rss_feed.dart';



@lazySingleton
class FeedRemoteDataSource {
  final Dio _dio;

  FeedRemoteDataSource(this._dio);
  Future<Feed> get(String url) async {
    final rawFeed = await _loadFromUrl(url);
    final feed = _rawToFeed(url, rawFeed);
    return feed;
  }

  Feed _rawToFeed(String url, Response<dynamic> raw) {
    final feedType = FeedIdentifier.identifyFeedType(raw.data);
    switch (feedType) {
      case FeedType.RSS:
        return Feed.fromRssFeed(url, RssFeed.parse(raw.data));
      case FeedType.Atom:
        return Feed.fromAtomFeed(url, AtomFeed.parse(raw.data));
    }
  }

  Future<Response<dynamic>> _loadFromUrl(String url) async {
    final response = await _dio.get(url);
    Dio().close();
    return response;
  }
}
