import 'dart:async';

import 'package:dio/dio.dart';
import '../../domain/feeds/feed_failure.dart';
import '../../domain/feeds/feed.dart';
import 'package:dartz/dartz.dart';
import '../../domain/feeds/i_feed_facade.dart';
import '../core/errors.dart';
import 'feed_remote_data_source.dart';
import 'package:injectable/injectable.dart';

import 'feed_local_data_source.dart';

@LazySingleton(as: IFeedFacade)
class FeedFacade implements IFeedFacade {
  final FeedRemoteDataSource _feedDataSource;
  final FeedLocalDataSource _feedLocalDataSource;
  Stream<Unit> get localFeedsStream => _feedLocalDataSource.feedStream.stream;

  FeedFacade(this._feedDataSource, this._feedLocalDataSource);
  @override
  Future<Either<FeedFailure, Feed>> feedFromUrl(String url) async {
    try {
      final feed = await _feedDataSource.get(url);
      return right(feed);
    } on FeedTypeError catch (_) {
      return left(FeedFailure.invalidFeed());
    } on DioError catch (_) {
      return left(FeedFailure.networkError());
    } catch (ex) {
      return left(FeedFailure.unknown(ex));
    }
  }

  @override
  Future<Either<FeedFailure, Unit>> save(Feed feed) async {
    try {
      await _feedLocalDataSource.save(feed);
      return right(unit);
    } catch (ex) {
      return left(FeedFailure.unknown(ex));
    }
  }

  @override
  Future<Either<FeedFailure, List<Feed>>> getAll() async {
    try {
      final feeds = await _feedLocalDataSource.getAll();
      return right(feeds);
    } catch (ex) {
      return left(FeedFailure.unknown(ex));
    }
  }

  @override
  Future<Either<FeedFailure, Feed>> get(String url) async {
    try {
      final feed = await _feedLocalDataSource.get(url: url);
      return right(feed);
    } on LocalStorageNotFoundError catch (_) {
      return left(FeedFailure.notFound());
    }
  }
}
