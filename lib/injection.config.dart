// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:dio/dio.dart' as _i3;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import 'application/feeds/add_feed_cubit.dart' as _i9;
import 'application/feeds/feeds_cubit.dart' as _i10;
import 'application/home/home_cubit.dart' as _i6;
import 'domain/feeds/i_feed_facade.dart' as _i7;
import 'infrastructure/core/injectable_module.dart' as _i11;
import 'infrastructure/feeds/feed_facade.dart' as _i8;
import 'infrastructure/feeds/feed_local_data_source.dart' as _i4;
import 'infrastructure/feeds/feed_remote_data_source.dart'
    as _i5; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  final injectableModule = _$InjectableModule();
  gh.lazySingleton<_i3.Dio>(() => injectableModule.dio());
  gh.lazySingleton<_i4.FeedLocalDataSource>(() => _i4.FeedLocalDataSource());
  gh.lazySingleton<_i5.FeedRemoteDataSource>(
      () => _i5.FeedRemoteDataSource(get<_i3.Dio>()));
  gh.lazySingleton<_i6.HomeCubit>(() => _i6.HomeCubit());
  gh.lazySingleton<_i7.IFeedFacade>(() => _i8.FeedFacade(
      get<_i5.FeedRemoteDataSource>(), get<_i4.FeedLocalDataSource>()));
  gh.factory<_i9.AddFeedCubit>(() => _i9.AddFeedCubit(get<_i7.IFeedFacade>()));
  gh.lazySingleton<_i10.FeedsCubit>(
      () => _i10.FeedsCubit(get<_i7.IFeedFacade>()));
  return get;
}

class _$InjectableModule extends _i11.InjectableModule {}
