import 'dart:io';

enum GenericPlatform { Desktop, Mobile, Unknown }

class AdaptiveTools {
  static GenericPlatform get platform {
    if (Platform.isFuchsia || Platform.isAndroid || Platform.isIOS) {
      return GenericPlatform.Mobile;
    } else if (Platform.isLinux || Platform.isMacOS || Platform.isWindows) {
      return GenericPlatform.Desktop;
    } else {
      return GenericPlatform.Unknown;
    }
  }
}
