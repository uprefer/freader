import 'package:flutter/foundation.dart';
import '../../domain/feeds/feed.dart';
import '../../domain/feeds/feed_failure.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'add_feed_state.freezed.dart';

@freezed
class AddFeedState with _$AddFeedState {
  const factory AddFeedState.initial() = _Initial;

  const factory AddFeedState.error(FeedFailure fail) = _Error;

  const factory AddFeedState.loadingFeed(String url) = _Loading;
  const factory AddFeedState.loaded(Feed feed) = _Loaded;
  const factory AddFeedState.saving(Feed feed) = _Saving;
  const factory AddFeedState.saved() = _Saved;
}
