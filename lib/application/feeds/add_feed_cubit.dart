import 'package:bloc/bloc.dart';
import 'add_feed_state.dart';
import '../../domain/feeds/feed.dart';
import '../../domain/feeds/i_feed_facade.dart';
import 'package:injectable/injectable.dart';

@injectable
class AddFeedCubit extends Cubit<AddFeedState> {
  final IFeedFacade _feedFacade;

  AddFeedCubit(this._feedFacade) : super(AddFeedState.initial());

  void checkFeed(String url) async {
    emit(AddFeedState.loadingFeed(url));
    final failOrSuccessFeed = await _feedFacade.feedFromUrl(url);
    failOrSuccessFeed.fold(
      (fail) => emit(AddFeedState.error(fail)),
      (feed) => emit(
        AddFeedState.loaded(feed),
      ),
    );
  }

  void addFeed(Feed feed) async {
    emit(AddFeedState.saving(feed));
    final failOrSuccessSave = await _feedFacade.save(feed);
    failOrSuccessSave.fold(
      (l) => emit(AddFeedState.error(l)),
      (_) => emit(AddFeedState.saved()),
    );
  }
}
