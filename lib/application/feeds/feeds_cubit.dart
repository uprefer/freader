import 'dart:async';

import 'package:bloc/bloc.dart';
import 'feeds_state.dart';
import '../../domain/feeds/feed.dart';
import '../../domain/feeds/i_feed_facade.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class FeedsCubit extends Cubit<FeedsState> {
  final IFeedFacade _feedFacade;
  FeedsCubit(this._feedFacade) : super(FeedsState.initial()) {
    _feedFacade.localFeedsStream.listen((event) {
      load();
    });
  }

  Future<void> load() async {
    final failOrfeeds = await _feedFacade.getAll();
    failOrfeeds.fold(
      (l) {
        emit(FeedsState.error(l));
      },
      (feeds) {
        feeds.sort((a, b) => a.title.compareTo(b.title));
        emit(FeedsState.loaded(feeds));
      },
    );
  }

  void selectFeed(Feed? selectedFeed) async {
    if (state is FeedStateLoaded) {
      emit(
        (state as FeedStateLoaded).copyWith(
          selectedFeed: selectedFeed,
        ),
      );
    }
  }
}
