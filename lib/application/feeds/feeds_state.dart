import 'package:flutter/foundation.dart';
import '../../domain/feeds/feed_failure.dart';
import '../../domain/feeds/feed.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'feeds_state.freezed.dart';

@freezed
class FeedsState with _$FeedsState {
  const factory FeedsState.initial() = _Initial;

  const factory FeedsState.loaded(List<Feed> feeds, {Feed? selectedFeed}) =
      FeedStateLoaded;

  const factory FeedsState.error(FeedFailure failure) = _Error;
}
