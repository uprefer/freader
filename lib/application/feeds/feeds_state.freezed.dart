// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'feeds_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$FeedsStateTearOff {
  const _$FeedsStateTearOff();

  _Initial initial() {
    return const _Initial();
  }

  FeedStateLoaded loaded(List<Feed> feeds, {Feed? selectedFeed}) {
    return FeedStateLoaded(
      feeds,
      selectedFeed: selectedFeed,
    );
  }

  _Error error(FeedFailure failure) {
    return _Error(
      failure,
    );
  }
}

/// @nodoc
const $FeedsState = _$FeedsStateTearOff();

/// @nodoc
mixin _$FeedsState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(List<Feed> feeds, Feed? selectedFeed) loaded,
    required TResult Function(FeedFailure failure) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(List<Feed> feeds, Feed? selectedFeed)? loaded,
    TResult Function(FeedFailure failure)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(FeedStateLoaded value) loaded,
    required TResult Function(_Error value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(FeedStateLoaded value)? loaded,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FeedsStateCopyWith<$Res> {
  factory $FeedsStateCopyWith(
          FeedsState value, $Res Function(FeedsState) then) =
      _$FeedsStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$FeedsStateCopyWithImpl<$Res> implements $FeedsStateCopyWith<$Res> {
  _$FeedsStateCopyWithImpl(this._value, this._then);

  final FeedsState _value;
  // ignore: unused_field
  final $Res Function(FeedsState) _then;
}

/// @nodoc
abstract class _$InitialCopyWith<$Res> {
  factory _$InitialCopyWith(_Initial value, $Res Function(_Initial) then) =
      __$InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$InitialCopyWithImpl<$Res> extends _$FeedsStateCopyWithImpl<$Res>
    implements _$InitialCopyWith<$Res> {
  __$InitialCopyWithImpl(_Initial _value, $Res Function(_Initial) _then)
      : super(_value, (v) => _then(v as _Initial));

  @override
  _Initial get _value => super._value as _Initial;
}

/// @nodoc

class _$_Initial with DiagnosticableTreeMixin implements _Initial {
  const _$_Initial();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'FeedsState.initial()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties..add(DiagnosticsProperty('type', 'FeedsState.initial'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(List<Feed> feeds, Feed? selectedFeed) loaded,
    required TResult Function(FeedFailure failure) error,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(List<Feed> feeds, Feed? selectedFeed)? loaded,
    TResult Function(FeedFailure failure)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(FeedStateLoaded value) loaded,
    required TResult Function(_Error value) error,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(FeedStateLoaded value)? loaded,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements FeedsState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class $FeedStateLoadedCopyWith<$Res> {
  factory $FeedStateLoadedCopyWith(
          FeedStateLoaded value, $Res Function(FeedStateLoaded) then) =
      _$FeedStateLoadedCopyWithImpl<$Res>;
  $Res call({List<Feed> feeds, Feed? selectedFeed});

  $FeedCopyWith<$Res>? get selectedFeed;
}

/// @nodoc
class _$FeedStateLoadedCopyWithImpl<$Res> extends _$FeedsStateCopyWithImpl<$Res>
    implements $FeedStateLoadedCopyWith<$Res> {
  _$FeedStateLoadedCopyWithImpl(
      FeedStateLoaded _value, $Res Function(FeedStateLoaded) _then)
      : super(_value, (v) => _then(v as FeedStateLoaded));

  @override
  FeedStateLoaded get _value => super._value as FeedStateLoaded;

  @override
  $Res call({
    Object? feeds = freezed,
    Object? selectedFeed = freezed,
  }) {
    return _then(FeedStateLoaded(
      feeds == freezed
          ? _value.feeds
          : feeds // ignore: cast_nullable_to_non_nullable
              as List<Feed>,
      selectedFeed: selectedFeed == freezed
          ? _value.selectedFeed
          : selectedFeed // ignore: cast_nullable_to_non_nullable
              as Feed?,
    ));
  }

  @override
  $FeedCopyWith<$Res>? get selectedFeed {
    if (_value.selectedFeed == null) {
      return null;
    }

    return $FeedCopyWith<$Res>(_value.selectedFeed!, (value) {
      return _then(_value.copyWith(selectedFeed: value));
    });
  }
}

/// @nodoc

class _$FeedStateLoaded
    with DiagnosticableTreeMixin
    implements FeedStateLoaded {
  const _$FeedStateLoaded(this.feeds, {this.selectedFeed});

  @override
  final List<Feed> feeds;
  @override
  final Feed? selectedFeed;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'FeedsState.loaded(feeds: $feeds, selectedFeed: $selectedFeed)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'FeedsState.loaded'))
      ..add(DiagnosticsProperty('feeds', feeds))
      ..add(DiagnosticsProperty('selectedFeed', selectedFeed));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is FeedStateLoaded &&
            (identical(other.feeds, feeds) ||
                const DeepCollectionEquality().equals(other.feeds, feeds)) &&
            (identical(other.selectedFeed, selectedFeed) ||
                const DeepCollectionEquality()
                    .equals(other.selectedFeed, selectedFeed)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(feeds) ^
      const DeepCollectionEquality().hash(selectedFeed);

  @JsonKey(ignore: true)
  @override
  $FeedStateLoadedCopyWith<FeedStateLoaded> get copyWith =>
      _$FeedStateLoadedCopyWithImpl<FeedStateLoaded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(List<Feed> feeds, Feed? selectedFeed) loaded,
    required TResult Function(FeedFailure failure) error,
  }) {
    return loaded(feeds, selectedFeed);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(List<Feed> feeds, Feed? selectedFeed)? loaded,
    TResult Function(FeedFailure failure)? error,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(feeds, selectedFeed);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(FeedStateLoaded value) loaded,
    required TResult Function(_Error value) error,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(FeedStateLoaded value)? loaded,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class FeedStateLoaded implements FeedsState {
  const factory FeedStateLoaded(List<Feed> feeds, {Feed? selectedFeed}) =
      _$FeedStateLoaded;

  List<Feed> get feeds => throw _privateConstructorUsedError;
  Feed? get selectedFeed => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $FeedStateLoadedCopyWith<FeedStateLoaded> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$ErrorCopyWith<$Res> {
  factory _$ErrorCopyWith(_Error value, $Res Function(_Error) then) =
      __$ErrorCopyWithImpl<$Res>;
  $Res call({FeedFailure failure});

  $FeedFailureCopyWith<$Res> get failure;
}

/// @nodoc
class __$ErrorCopyWithImpl<$Res> extends _$FeedsStateCopyWithImpl<$Res>
    implements _$ErrorCopyWith<$Res> {
  __$ErrorCopyWithImpl(_Error _value, $Res Function(_Error) _then)
      : super(_value, (v) => _then(v as _Error));

  @override
  _Error get _value => super._value as _Error;

  @override
  $Res call({
    Object? failure = freezed,
  }) {
    return _then(_Error(
      failure == freezed
          ? _value.failure
          : failure // ignore: cast_nullable_to_non_nullable
              as FeedFailure,
    ));
  }

  @override
  $FeedFailureCopyWith<$Res> get failure {
    return $FeedFailureCopyWith<$Res>(_value.failure, (value) {
      return _then(_value.copyWith(failure: value));
    });
  }
}

/// @nodoc

class _$_Error with DiagnosticableTreeMixin implements _Error {
  const _$_Error(this.failure);

  @override
  final FeedFailure failure;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'FeedsState.error(failure: $failure)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'FeedsState.error'))
      ..add(DiagnosticsProperty('failure', failure));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Error &&
            (identical(other.failure, failure) ||
                const DeepCollectionEquality().equals(other.failure, failure)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(failure);

  @JsonKey(ignore: true)
  @override
  _$ErrorCopyWith<_Error> get copyWith =>
      __$ErrorCopyWithImpl<_Error>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(List<Feed> feeds, Feed? selectedFeed) loaded,
    required TResult Function(FeedFailure failure) error,
  }) {
    return error(failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(List<Feed> feeds, Feed? selectedFeed)? loaded,
    TResult Function(FeedFailure failure)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(FeedStateLoaded value) loaded,
    required TResult Function(_Error value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(FeedStateLoaded value)? loaded,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _Error implements FeedsState {
  const factory _Error(FeedFailure failure) = _$_Error;

  FeedFailure get failure => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$ErrorCopyWith<_Error> get copyWith => throw _privateConstructorUsedError;
}
