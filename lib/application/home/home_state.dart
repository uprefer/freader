import '../../domain/feeds/feed.dart';

import '../core/adaptive_tools.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'home_state.freezed.dart';

@freezed
class HomeState with _$HomeState {
  const factory HomeState(
      {@Default(false) bool leftMenuOpened,
      required GenericPlatform platform,
      Feed? selectedFeed}) = _HomeState;
}
