// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'home_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$HomeStateTearOff {
  const _$HomeStateTearOff();

  _HomeState call(
      {bool leftMenuOpened = false,
      required GenericPlatform platform,
      Feed? selectedFeed}) {
    return _HomeState(
      leftMenuOpened: leftMenuOpened,
      platform: platform,
      selectedFeed: selectedFeed,
    );
  }
}

/// @nodoc
const $HomeState = _$HomeStateTearOff();

/// @nodoc
mixin _$HomeState {
  bool get leftMenuOpened => throw _privateConstructorUsedError;
  GenericPlatform get platform => throw _privateConstructorUsedError;
  Feed? get selectedFeed => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $HomeStateCopyWith<HomeState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HomeStateCopyWith<$Res> {
  factory $HomeStateCopyWith(HomeState value, $Res Function(HomeState) then) =
      _$HomeStateCopyWithImpl<$Res>;
  $Res call(
      {bool leftMenuOpened, GenericPlatform platform, Feed? selectedFeed});

  $FeedCopyWith<$Res>? get selectedFeed;
}

/// @nodoc
class _$HomeStateCopyWithImpl<$Res> implements $HomeStateCopyWith<$Res> {
  _$HomeStateCopyWithImpl(this._value, this._then);

  final HomeState _value;
  // ignore: unused_field
  final $Res Function(HomeState) _then;

  @override
  $Res call({
    Object? leftMenuOpened = freezed,
    Object? platform = freezed,
    Object? selectedFeed = freezed,
  }) {
    return _then(_value.copyWith(
      leftMenuOpened: leftMenuOpened == freezed
          ? _value.leftMenuOpened
          : leftMenuOpened // ignore: cast_nullable_to_non_nullable
              as bool,
      platform: platform == freezed
          ? _value.platform
          : platform // ignore: cast_nullable_to_non_nullable
              as GenericPlatform,
      selectedFeed: selectedFeed == freezed
          ? _value.selectedFeed
          : selectedFeed // ignore: cast_nullable_to_non_nullable
              as Feed?,
    ));
  }

  @override
  $FeedCopyWith<$Res>? get selectedFeed {
    if (_value.selectedFeed == null) {
      return null;
    }

    return $FeedCopyWith<$Res>(_value.selectedFeed!, (value) {
      return _then(_value.copyWith(selectedFeed: value));
    });
  }
}

/// @nodoc
abstract class _$HomeStateCopyWith<$Res> implements $HomeStateCopyWith<$Res> {
  factory _$HomeStateCopyWith(
          _HomeState value, $Res Function(_HomeState) then) =
      __$HomeStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {bool leftMenuOpened, GenericPlatform platform, Feed? selectedFeed});

  @override
  $FeedCopyWith<$Res>? get selectedFeed;
}

/// @nodoc
class __$HomeStateCopyWithImpl<$Res> extends _$HomeStateCopyWithImpl<$Res>
    implements _$HomeStateCopyWith<$Res> {
  __$HomeStateCopyWithImpl(_HomeState _value, $Res Function(_HomeState) _then)
      : super(_value, (v) => _then(v as _HomeState));

  @override
  _HomeState get _value => super._value as _HomeState;

  @override
  $Res call({
    Object? leftMenuOpened = freezed,
    Object? platform = freezed,
    Object? selectedFeed = freezed,
  }) {
    return _then(_HomeState(
      leftMenuOpened: leftMenuOpened == freezed
          ? _value.leftMenuOpened
          : leftMenuOpened // ignore: cast_nullable_to_non_nullable
              as bool,
      platform: platform == freezed
          ? _value.platform
          : platform // ignore: cast_nullable_to_non_nullable
              as GenericPlatform,
      selectedFeed: selectedFeed == freezed
          ? _value.selectedFeed
          : selectedFeed // ignore: cast_nullable_to_non_nullable
              as Feed?,
    ));
  }
}

/// @nodoc

class _$_HomeState implements _HomeState {
  const _$_HomeState(
      {this.leftMenuOpened = false, required this.platform, this.selectedFeed});

  @JsonKey(defaultValue: false)
  @override
  final bool leftMenuOpened;
  @override
  final GenericPlatform platform;
  @override
  final Feed? selectedFeed;

  @override
  String toString() {
    return 'HomeState(leftMenuOpened: $leftMenuOpened, platform: $platform, selectedFeed: $selectedFeed)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _HomeState &&
            (identical(other.leftMenuOpened, leftMenuOpened) ||
                const DeepCollectionEquality()
                    .equals(other.leftMenuOpened, leftMenuOpened)) &&
            (identical(other.platform, platform) ||
                const DeepCollectionEquality()
                    .equals(other.platform, platform)) &&
            (identical(other.selectedFeed, selectedFeed) ||
                const DeepCollectionEquality()
                    .equals(other.selectedFeed, selectedFeed)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(leftMenuOpened) ^
      const DeepCollectionEquality().hash(platform) ^
      const DeepCollectionEquality().hash(selectedFeed);

  @JsonKey(ignore: true)
  @override
  _$HomeStateCopyWith<_HomeState> get copyWith =>
      __$HomeStateCopyWithImpl<_HomeState>(this, _$identity);
}

abstract class _HomeState implements HomeState {
  const factory _HomeState(
      {bool leftMenuOpened,
      required GenericPlatform platform,
      Feed? selectedFeed}) = _$_HomeState;

  @override
  bool get leftMenuOpened => throw _privateConstructorUsedError;
  @override
  GenericPlatform get platform => throw _privateConstructorUsedError;
  @override
  Feed? get selectedFeed => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$HomeStateCopyWith<_HomeState> get copyWith =>
      throw _privateConstructorUsedError;
}
