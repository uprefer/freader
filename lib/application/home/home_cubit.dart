import 'package:bloc/bloc.dart';
import '../core/adaptive_tools.dart';
import 'package:injectable/injectable.dart';

import 'home_state.dart';

@lazySingleton
class HomeCubit extends Cubit<HomeState> {
  HomeCubit() : super(HomeState(platform: AdaptiveTools.platform));

  void switchMenu() {
    emit(state.copyWith(leftMenuOpened: !state.leftMenuOpened));
  }
}
