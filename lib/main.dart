import 'package:flutter/material.dart';
import 'injection.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'presentation/core/router.gr.dart';

void main() async {
  await Hive.initFlutter();
  configureDependencies();
  runApp(FReader());
}

class FReader extends StatelessWidget {
  final _appRouter = FReaderRouter();

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routeInformationParser: _appRouter.defaultRouteParser(),
      routerDelegate: _appRouter.delegate(),
      title: "FReader",
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity),
    );
  }
}
