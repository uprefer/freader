import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../application/core/adaptive_tools.dart';
import '../application/feeds/feeds_cubit.dart';
import '../application/home/home_cubit.dart';
import '../application/home/home_state.dart';
import '../injection.dart';
import 'home/desktop/home.dart';
import 'home/mobile/home.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<HomeCubit>(
          create: (context) => getIt<HomeCubit>(),
        ),
        BlocProvider<FeedsCubit>(
          create: (context) => getIt<FeedsCubit>()..load(),
        ),
      ],
      child: HomeBody(),
    );
  }
}

class HomeBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(builder: (context, state) {
      switch (state.platform) {
        case GenericPlatform.Desktop:
          return DesktopHome();
        case GenericPlatform.Mobile:
          return MobileHome();
        case GenericPlatform.Unknown:
          return Placeholder();
      }
    });
  }
}
