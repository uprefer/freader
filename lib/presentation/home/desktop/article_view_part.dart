import 'package:flutter/material.dart';

class ArticleViewPart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            child: Placeholder(
              color: Colors.red,
            ),
            flex: 2,
          ),
          Expanded(
            child: Placeholder(),
            flex: 7,
          )
        ],
      ),
    );
  }
}
