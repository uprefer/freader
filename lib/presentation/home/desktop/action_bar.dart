import 'package:flutter/material.dart';
import '../../../application/feeds/feeds_cubit.dart';
import '../../../application/feeds/feeds_state.dart';
import '../../../application/home/home_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'home.dart';

class DesktopHomeActionBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FeedsCubit, FeedsState>(builder: (context, _) {
      return Container(
        height: 40,
        color: Colors.blue,
        child: Row(
          children: [
            if (context.read<HomeCubit>().state.leftMenuOpened)
              Container(
                  width: kLeftMenuWidth,
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: IconButton(
                      icon: Icon(Icons.menu),
                      onPressed: () {
                        context.read<HomeCubit>().switchMenu();
                      },
                    ),
                  )),
            if (!context.read<HomeCubit>().state.leftMenuOpened)
              Expanded(
                flex: kArticleListFlex,
                child: Row(
                  children: [
                    Container(
                        child: Align(
                      alignment: Alignment.centerLeft,
                      child: IconButton(
                        icon: Icon(Icons.menu),
                        onPressed: () {
                          context.read<HomeCubit>().switchMenu();
                        },
                      ),
                    )),
                    SizedBox(
                      width: 10,
                    ),
                    context.read<FeedsCubit>().state.maybeMap(
                        loaded: (loadedS) {
                          return loadedS.selectedFeed != null
                              ? Text(loadedS.selectedFeed!.title)
                              : Text("All");
                        },
                        orElse: () => Container()),
                  ],
                ),
              ),
            if (context.read<HomeCubit>().state.leftMenuOpened)
              Expanded(
                  flex: kArticleListFlex,
                  child: Center(
                    child: context.read<FeedsCubit>().state.maybeMap(
                        loaded: (loadedS) {
                          return loadedS.selectedFeed != null
                              ? Text(loadedS.selectedFeed!.title)
                              : Text("All");
                        },
                        orElse: () => Container()),
                  )),
            Expanded(
              child: Placeholder(),
              flex: 7,
            ),
          ],
        ),
      );
    });
  }
}
