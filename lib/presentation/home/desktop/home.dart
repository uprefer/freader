import 'package:flutter/material.dart';
import 'action_bar.dart';
import 'home_body.dart';

const double kLeftMenuWidth = 200;
const int kArticleListFlex = 2;

class DesktopHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        DesktopHomeActionBar(),
        Expanded(child: DesktopHomeBody()),
      ],
    ));
  }
}
