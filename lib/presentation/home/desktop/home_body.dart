import 'package:flutter/material.dart';
import '../../../application/home/home_cubit.dart';
import 'article_view_part.dart';
import 'left_menu.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

const double _leftMenuWidth = 200;

class DesktopHomeBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        if (context.read<HomeCubit>().state.leftMenuOpened)
          Container(
            color: Colors.blue,
            child: LeftMenuWidget(),
            width: _leftMenuWidth,
          ),
        Expanded(child: ArticleViewPart())
      ],
    );
  }
}
