import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../application/feeds/feeds_cubit.dart';
import '../../../application/feeds/feeds_state.dart';
import '../../feeds/feed_list.dart';

class LeftMenuWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        child: BlocBuilder<FeedsCubit, FeedsState>(builder: (context, state) {
          return state.maybeMap(
              loaded: (lState) {
                return Column(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    ListTile(
                      title: Text("All"),
                      leading: Icon(Icons.text_snippet_outlined),
                      onTap: () {
                        context.read<FeedsCubit>().selectFeed(null);
                      },
                    ),
                    lState.feeds.isEmpty
                        ? Expanded(child: Placeholder())
                        : Expanded(child: FeedList(lState.feeds)),
                  ],
                );
              },
              orElse: () => Placeholder());
        }),
      ),
    );
  }
}
