import 'package:flutter/material.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freader/presentation/home/mobile/drawer.dart';
import '../../feeds/feed_list.dart';
import '../../../application/feeds/feeds_cubit.dart';
import '../../../application/feeds/feeds_state.dart';
import '../../core/router.gr.dart';

class MobileHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("fReader Mobile"),
      ),
      drawer: MobileDrawer(),
      body: _MobileHomeBody(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          context.router.push(AddFeedScreenRoute());
        },
        child: Icon(Icons.add),
      ),
    );
  }
}

class _MobileHomeBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FeedsCubit, FeedsState>(builder: (context, state) {
      return state.maybeMap(
          error: (errState) {
            return Center(
              child: Text(errState.failure.toString()),
            );
          },
          loaded: (loadedS) {
            if (loadedS.feeds.isEmpty) {
              return Center(
                child: Text("Aucun flux. Pensez a en ajouter ;)"),
              );
            }
            return FeedList(loadedS.feeds);
          },
          orElse: () => Center(
                child: Container(
                  width: 50,
                  height: 50,
                  child: CircularProgressIndicator(),
                ),
              ));
    });
  }
}
