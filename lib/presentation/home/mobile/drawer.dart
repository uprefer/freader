import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../application/feeds/feeds_cubit.dart';
import '../../../application/feeds/feeds_state.dart';
import '../../feeds/feed_list.dart';

class MobileDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer();
  }
}
