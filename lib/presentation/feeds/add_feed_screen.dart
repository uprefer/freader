import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../application/feeds/add_feed_cubit.dart';
import '../../application/feeds/add_feed_state.dart';
import '../../domain/feeds/feed.dart';
import 'package:validators/validators.dart' as validator;
import 'package:auto_route/auto_route.dart';

import '../../injection.dart';

class AddFeedScreen extends StatefulWidget {
  @override
  _AddFeedScreenState createState() => _AddFeedScreenState();
}

class _AddFeedScreenState extends State<AddFeedScreen> {
  final _feedUrlCtrl = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Ajouter un flux"),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: BlocProvider<AddFeedCubit>(
          create: (context) => getIt<AddFeedCubit>(),
          child: BlocConsumer<AddFeedCubit, AddFeedState>(
            listener: (context, state) {
              state.maybeMap(
                saved: (_) {
                  context.router.pop();
                },
                orElse: () {},
              );
            },
            builder: (context, state) {
              return Material(
                color: Colors.transparent,
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  width: MediaQuery.of(context).size.width * 0.5,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 20),
                        child: Form(
                          key: _formKey,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              TextFormField(
                                autovalidateMode:
                                    AutovalidateMode.onUserInteraction,
                                controller: _feedUrlCtrl,
                                decoration:
                                    InputDecoration(labelText: "Url du flux"),
                                validator: (value) {
                                  if (validator.isURL(value)) {
                                    return null;
                                  } else {
                                    return 'This is not an url';
                                  }
                                },
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              ElevatedButton(
                                  onPressed: () {
                                    final formState = _formKey.currentState;
                                    if (formState != null &&
                                        formState.validate()) {
                                      context
                                          .read<AddFeedCubit>()
                                          .checkFeed(_feedUrlCtrl.text);
                                    }
                                  },
                                  child: Text("Load feed"))
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: state.maybeMap(
                          loadingFeed: (loadingState) {
                            return Container(
                              width: 100,
                              height: 100,
                              child: Center(child: CircularProgressIndicator()),
                            );
                          },
                          loaded: (feedState) {
                            return _FeedDetails(
                              feed: feedState.feed,
                            );
                          },
                          saving: (_) {
                            return Container(
                              width: 100,
                              height: 100,
                              child: Center(child: CircularProgressIndicator()),
                            );
                          },
                          error: (ex) {
                            return Container(
                              width: 100,
                              height: 100,
                              child: Center(
                                  child: Text("Can't find rss at this url")),
                            );
                          },
                          orElse: () {
                            return Container();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

class _FeedDetails extends StatefulWidget {
  final Feed feed;

  const _FeedDetails({Key? key, required this.feed}) : super(key: key);
  @override
  __FeedDetailsState createState() => __FeedDetailsState();
}

class __FeedDetailsState extends State<_FeedDetails> {
  final _titleCtrl = TextEditingController();
  final _descCtrl = TextEditingController();

  @override
  void initState() {
    super.initState();
    _titleCtrl.text = widget.feed.title;
    _descCtrl.text = widget.feed.subTitle ?? '';
  }

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      thickness: 5,
      showTrackOnHover: true,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              if (widget.feed.iconUrl != null &&
                  widget.feed.iconUrl!.characters.last == '/')
                Image.network(widget.feed.iconUrl!
                    .substring(0, widget.feed.iconUrl!.length - 1)),
              if (widget.feed.iconUrl != null &&
                  widget.feed.iconUrl!.characters.last != '/')
                Image.network(widget.feed.iconUrl!),
              TextFormField(
                controller: _titleCtrl,
                decoration: InputDecoration(labelText: "Titre"),
                enabled: false,
              ),
              TextFormField(
                controller: _descCtrl,
                decoration: InputDecoration(labelText: "Description"),
                enabled: false,
              ),
              SizedBox(
                height: 25,
              ),
              context.read<AddFeedCubit>().state.maybeMap(
                    loaded: (feedState) => ElevatedButton(
                        onPressed: () {
                          context.read<AddFeedCubit>().addFeed(widget.feed);
                        },
                        child: Text("Add")),
                    orElse: () => SizedBox(),
                  ),
            ],
          ),
        ),
      ),
    );
  }
}
