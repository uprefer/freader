import 'package:flutter/material.dart';
import '../../application/feeds/feeds_cubit.dart';
import '../../domain/feeds/feed.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FeedList extends StatelessWidget {
  final List<Feed> _feedList;

  const FeedList(
    this._feedList, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: _feedList.map((e) => FeedTile(e)).toList(),
    );
  }
}

class FeedTile extends StatelessWidget {
  final Feed _feed;

  const FeedTile(
    this._feed, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () {
        context.read<FeedsCubit>().selectFeed(_feed);
      },
      title: Text(_feed.title),
      leading: _feed.iconUrl != null
          ? Image.network(
              _feed.iconUrl!,
              width: 20,
              height: 20,
            )
          : Container(
              width: 20,
              height: 20,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.black,
              ),
              child: Center(
                child: Text(
                  "${_feed.title.substring(0, 1)}",
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
    );
  }
}
