// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:auto_route/auto_route.dart' as _i1;
import 'package:flutter/material.dart' as _i2;

import '../feeds/add_feed_screen.dart' as _i4;
import '../home_screen.dart' as _i3;

class FReaderRouter extends _i1.RootStackRouter {
  FReaderRouter([_i2.GlobalKey<_i2.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i1.PageFactory> pagesMap = {
    HomeScreenRoute.name: (routeData) {
      return _i1.MaterialPageX<dynamic>(
          routeData: routeData, child: _i3.HomeScreen());
    },
    AddFeedScreenRoute.name: (routeData) {
      return _i1.MaterialPageX<dynamic>(
          routeData: routeData, child: _i4.AddFeedScreen());
    }
  };

  @override
  List<_i1.RouteConfig> get routes => [
        _i1.RouteConfig('/#redirect',
            path: '/', redirectTo: 'home', fullMatch: true),
        _i1.RouteConfig(HomeScreenRoute.name, path: 'home'),
        _i1.RouteConfig(AddFeedScreenRoute.name, path: '/feeds/add')
      ];
}

class HomeScreenRoute extends _i1.PageRouteInfo {
  const HomeScreenRoute() : super(name, path: 'home');

  static const String name = 'HomeScreenRoute';
}

class AddFeedScreenRoute extends _i1.PageRouteInfo {
  const AddFeedScreenRoute() : super(name, path: '/feeds/add');

  static const String name = 'AddFeedScreenRoute';
}
