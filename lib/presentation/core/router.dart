import 'package:auto_route/annotations.dart';
import '../feeds/add_feed_screen.dart';
import '../home_screen.dart';

@MaterialAutoRouter(routes: [
  AutoRoute(page: HomeScreen, initial: true, path: 'home'),
  AutoRoute(
    page: AddFeedScreen,
    path: '/feeds/add',
  )
])
class $FReaderRouter {}
