// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feed.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Feed _$_$_FeedFromJson(Map<String, dynamic> json) {
  return _$_Feed(
    url: json['url'] as String,
    type: _$enumDecode(_$FeedTypeEnumMap, json['type']),
    iconUrl: json['iconUrl'] as String?,
    title: json['title'] as String,
    subTitle: json['subTitle'] as String?,
    updated: json['updated'] == null
        ? null
        : DateTime.parse(json['updated'] as String),
  );
}

Map<String, dynamic> _$_$_FeedToJson(_$_Feed instance) => <String, dynamic>{
      'url': instance.url,
      'type': _$FeedTypeEnumMap[instance.type],
      'iconUrl': instance.iconUrl,
      'title': instance.title,
      'subTitle': instance.subTitle,
      'updated': instance.updated?.toIso8601String(),
    };

K _$enumDecode<K, V>(
  Map<K, V> enumValues,
  Object? source, {
  K? unknownValue,
}) {
  if (source == null) {
    throw ArgumentError(
      'A value must be provided. Supported values: '
      '${enumValues.values.join(', ')}',
    );
  }

  return enumValues.entries.singleWhere(
    (e) => e.value == source,
    orElse: () {
      if (unknownValue == null) {
        throw ArgumentError(
          '`$source` is not one of the supported values: '
          '${enumValues.values.join(', ')}',
        );
      }
      return MapEntry(unknownValue, enumValues.values.first);
    },
  ).key;
}

const _$FeedTypeEnumMap = {
  FeedType.RSS: 'RSS',
  FeedType.Atom: 'Atom',
};
