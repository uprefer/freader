import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
part 'feed_failure.freezed.dart';

@freezed
class FeedFailure with _$FeedFailure {
  const factory FeedFailure.unknown(Object? ex) = _Unknown;
  const factory FeedFailure.networkError() = _NetworkError;
  const factory FeedFailure.notFound() = _NotFound;
  const factory FeedFailure.invalidFeed() = _Invalid;
}
