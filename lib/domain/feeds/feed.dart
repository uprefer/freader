import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:webfeed/domain/atom_feed.dart';
import 'package:webfeed/domain/rss_feed.dart';

part 'feed.freezed.dart';
part 'feed.g.dart';

enum FeedType { RSS, Atom }

@freezed
class Feed with _$Feed {
  const factory Feed({
    required String url,
    required FeedType type,
    String? iconUrl,
    required String title,
    String? subTitle,
    DateTime? updated,
  }) = _Feed;

  factory Feed.fromParsedFeed(String url, dynamic parsed) => parsed is AtomFeed
      ? Feed.fromAtomFeed(url, parsed)
      : Feed.fromRssFeed(url, parsed);

  factory Feed.fromAtomFeed(String url, AtomFeed parsed) => Feed(
      url: url,
      type: FeedType.Atom,
      iconUrl: parsed.icon,
      title: parsed.title!,
      subTitle: parsed.subtitle,
      updated: parsed.updated);
  factory Feed.fromRssFeed(String url, RssFeed parsed) => Feed(
      url: url,
      type: FeedType.RSS,
      iconUrl: parsed.image?.url,
      title: parsed.title!,
      subTitle: parsed.description,
      updated: DateTime.tryParse(
        parsed.lastBuildDate ?? "",
      ));

  factory Feed.fromJson(Map<String, dynamic> json) => _$FeedFromJson(json);
}
