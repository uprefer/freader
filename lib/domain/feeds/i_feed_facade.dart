import 'package:dartz/dartz.dart';
import 'feed.dart';
import 'feed_failure.dart';

abstract class IFeedFacade {
  Stream<Unit> get localFeedsStream;
  Future<Either<FeedFailure, Feed>> feedFromUrl(String url);

  Future<Either<FeedFailure, Unit>> save(Feed feed);

  Future<Either<FeedFailure, List<Feed>>> getAll();
  Future<Either<FeedFailure, Feed>> get(String url);
}
