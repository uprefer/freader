// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'feed_failure.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$FeedFailureTearOff {
  const _$FeedFailureTearOff();

  _Unknown unknown(Object? ex) {
    return _Unknown(
      ex,
    );
  }

  _NetworkError networkError() {
    return const _NetworkError();
  }

  _NotFound notFound() {
    return const _NotFound();
  }

  _Invalid invalidFeed() {
    return const _Invalid();
  }
}

/// @nodoc
const $FeedFailure = _$FeedFailureTearOff();

/// @nodoc
mixin _$FeedFailure {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Object? ex) unknown,
    required TResult Function() networkError,
    required TResult Function() notFound,
    required TResult Function() invalidFeed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Object? ex)? unknown,
    TResult Function()? networkError,
    TResult Function()? notFound,
    TResult Function()? invalidFeed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unknown value) unknown,
    required TResult Function(_NetworkError value) networkError,
    required TResult Function(_NotFound value) notFound,
    required TResult Function(_Invalid value) invalidFeed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unknown value)? unknown,
    TResult Function(_NetworkError value)? networkError,
    TResult Function(_NotFound value)? notFound,
    TResult Function(_Invalid value)? invalidFeed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FeedFailureCopyWith<$Res> {
  factory $FeedFailureCopyWith(
          FeedFailure value, $Res Function(FeedFailure) then) =
      _$FeedFailureCopyWithImpl<$Res>;
}

/// @nodoc
class _$FeedFailureCopyWithImpl<$Res> implements $FeedFailureCopyWith<$Res> {
  _$FeedFailureCopyWithImpl(this._value, this._then);

  final FeedFailure _value;
  // ignore: unused_field
  final $Res Function(FeedFailure) _then;
}

/// @nodoc
abstract class _$UnknownCopyWith<$Res> {
  factory _$UnknownCopyWith(_Unknown value, $Res Function(_Unknown) then) =
      __$UnknownCopyWithImpl<$Res>;
  $Res call({Object? ex});
}

/// @nodoc
class __$UnknownCopyWithImpl<$Res> extends _$FeedFailureCopyWithImpl<$Res>
    implements _$UnknownCopyWith<$Res> {
  __$UnknownCopyWithImpl(_Unknown _value, $Res Function(_Unknown) _then)
      : super(_value, (v) => _then(v as _Unknown));

  @override
  _Unknown get _value => super._value as _Unknown;

  @override
  $Res call({
    Object? ex = freezed,
  }) {
    return _then(_Unknown(
      ex == freezed ? _value.ex : ex,
    ));
  }
}

/// @nodoc

class _$_Unknown with DiagnosticableTreeMixin implements _Unknown {
  const _$_Unknown(this.ex);

  @override
  final Object? ex;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'FeedFailure.unknown(ex: $ex)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'FeedFailure.unknown'))
      ..add(DiagnosticsProperty('ex', ex));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Unknown &&
            (identical(other.ex, ex) ||
                const DeepCollectionEquality().equals(other.ex, ex)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(ex);

  @JsonKey(ignore: true)
  @override
  _$UnknownCopyWith<_Unknown> get copyWith =>
      __$UnknownCopyWithImpl<_Unknown>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Object? ex) unknown,
    required TResult Function() networkError,
    required TResult Function() notFound,
    required TResult Function() invalidFeed,
  }) {
    return unknown(ex);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Object? ex)? unknown,
    TResult Function()? networkError,
    TResult Function()? notFound,
    TResult Function()? invalidFeed,
    required TResult orElse(),
  }) {
    if (unknown != null) {
      return unknown(ex);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unknown value) unknown,
    required TResult Function(_NetworkError value) networkError,
    required TResult Function(_NotFound value) notFound,
    required TResult Function(_Invalid value) invalidFeed,
  }) {
    return unknown(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unknown value)? unknown,
    TResult Function(_NetworkError value)? networkError,
    TResult Function(_NotFound value)? notFound,
    TResult Function(_Invalid value)? invalidFeed,
    required TResult orElse(),
  }) {
    if (unknown != null) {
      return unknown(this);
    }
    return orElse();
  }
}

abstract class _Unknown implements FeedFailure {
  const factory _Unknown(Object? ex) = _$_Unknown;

  Object? get ex => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$UnknownCopyWith<_Unknown> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$NetworkErrorCopyWith<$Res> {
  factory _$NetworkErrorCopyWith(
          _NetworkError value, $Res Function(_NetworkError) then) =
      __$NetworkErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$NetworkErrorCopyWithImpl<$Res> extends _$FeedFailureCopyWithImpl<$Res>
    implements _$NetworkErrorCopyWith<$Res> {
  __$NetworkErrorCopyWithImpl(
      _NetworkError _value, $Res Function(_NetworkError) _then)
      : super(_value, (v) => _then(v as _NetworkError));

  @override
  _NetworkError get _value => super._value as _NetworkError;
}

/// @nodoc

class _$_NetworkError with DiagnosticableTreeMixin implements _NetworkError {
  const _$_NetworkError();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'FeedFailure.networkError()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties..add(DiagnosticsProperty('type', 'FeedFailure.networkError'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _NetworkError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Object? ex) unknown,
    required TResult Function() networkError,
    required TResult Function() notFound,
    required TResult Function() invalidFeed,
  }) {
    return networkError();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Object? ex)? unknown,
    TResult Function()? networkError,
    TResult Function()? notFound,
    TResult Function()? invalidFeed,
    required TResult orElse(),
  }) {
    if (networkError != null) {
      return networkError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unknown value) unknown,
    required TResult Function(_NetworkError value) networkError,
    required TResult Function(_NotFound value) notFound,
    required TResult Function(_Invalid value) invalidFeed,
  }) {
    return networkError(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unknown value)? unknown,
    TResult Function(_NetworkError value)? networkError,
    TResult Function(_NotFound value)? notFound,
    TResult Function(_Invalid value)? invalidFeed,
    required TResult orElse(),
  }) {
    if (networkError != null) {
      return networkError(this);
    }
    return orElse();
  }
}

abstract class _NetworkError implements FeedFailure {
  const factory _NetworkError() = _$_NetworkError;
}

/// @nodoc
abstract class _$NotFoundCopyWith<$Res> {
  factory _$NotFoundCopyWith(_NotFound value, $Res Function(_NotFound) then) =
      __$NotFoundCopyWithImpl<$Res>;
}

/// @nodoc
class __$NotFoundCopyWithImpl<$Res> extends _$FeedFailureCopyWithImpl<$Res>
    implements _$NotFoundCopyWith<$Res> {
  __$NotFoundCopyWithImpl(_NotFound _value, $Res Function(_NotFound) _then)
      : super(_value, (v) => _then(v as _NotFound));

  @override
  _NotFound get _value => super._value as _NotFound;
}

/// @nodoc

class _$_NotFound with DiagnosticableTreeMixin implements _NotFound {
  const _$_NotFound();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'FeedFailure.notFound()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties..add(DiagnosticsProperty('type', 'FeedFailure.notFound'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _NotFound);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Object? ex) unknown,
    required TResult Function() networkError,
    required TResult Function() notFound,
    required TResult Function() invalidFeed,
  }) {
    return notFound();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Object? ex)? unknown,
    TResult Function()? networkError,
    TResult Function()? notFound,
    TResult Function()? invalidFeed,
    required TResult orElse(),
  }) {
    if (notFound != null) {
      return notFound();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unknown value) unknown,
    required TResult Function(_NetworkError value) networkError,
    required TResult Function(_NotFound value) notFound,
    required TResult Function(_Invalid value) invalidFeed,
  }) {
    return notFound(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unknown value)? unknown,
    TResult Function(_NetworkError value)? networkError,
    TResult Function(_NotFound value)? notFound,
    TResult Function(_Invalid value)? invalidFeed,
    required TResult orElse(),
  }) {
    if (notFound != null) {
      return notFound(this);
    }
    return orElse();
  }
}

abstract class _NotFound implements FeedFailure {
  const factory _NotFound() = _$_NotFound;
}

/// @nodoc
abstract class _$InvalidCopyWith<$Res> {
  factory _$InvalidCopyWith(_Invalid value, $Res Function(_Invalid) then) =
      __$InvalidCopyWithImpl<$Res>;
}

/// @nodoc
class __$InvalidCopyWithImpl<$Res> extends _$FeedFailureCopyWithImpl<$Res>
    implements _$InvalidCopyWith<$Res> {
  __$InvalidCopyWithImpl(_Invalid _value, $Res Function(_Invalid) _then)
      : super(_value, (v) => _then(v as _Invalid));

  @override
  _Invalid get _value => super._value as _Invalid;
}

/// @nodoc

class _$_Invalid with DiagnosticableTreeMixin implements _Invalid {
  const _$_Invalid();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'FeedFailure.invalidFeed()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties..add(DiagnosticsProperty('type', 'FeedFailure.invalidFeed'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Invalid);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Object? ex) unknown,
    required TResult Function() networkError,
    required TResult Function() notFound,
    required TResult Function() invalidFeed,
  }) {
    return invalidFeed();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Object? ex)? unknown,
    TResult Function()? networkError,
    TResult Function()? notFound,
    TResult Function()? invalidFeed,
    required TResult orElse(),
  }) {
    if (invalidFeed != null) {
      return invalidFeed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unknown value) unknown,
    required TResult Function(_NetworkError value) networkError,
    required TResult Function(_NotFound value) notFound,
    required TResult Function(_Invalid value) invalidFeed,
  }) {
    return invalidFeed(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unknown value)? unknown,
    TResult Function(_NetworkError value)? networkError,
    TResult Function(_NotFound value)? notFound,
    TResult Function(_Invalid value)? invalidFeed,
    required TResult orElse(),
  }) {
    if (invalidFeed != null) {
      return invalidFeed(this);
    }
    return orElse();
  }
}

abstract class _Invalid implements FeedFailure {
  const factory _Invalid() = _$_Invalid;
}
