fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew install fastlane`

# Available Actions
### setup
```
fastlane setup
```

### cleanup
```
fastlane cleanup
```

### build_deploy
```
fastlane build_deploy
```

### build_android
```
fastlane build_android
```

### build_android_integration
```
fastlane build_android_integration
```

### build_ios
```
fastlane build_ios
```

### deploy_android
```
fastlane deploy_android
```

### deploy_ios
```
fastlane deploy_ios
```


----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
